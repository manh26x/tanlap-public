# stage 1 as builder
FROM httpd:alpine

MAINTAINER mike root@103.81.87.190

RUN rm -r /usr/local/apache2/htdocs/*


# Copy all the files from the docker build context into the public htdocs of the apache container.
COPY ./ /usr/local/apache2/htdocs/

