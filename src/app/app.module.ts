import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {SidebarModule} from "primeng/sidebar";
import {ButtonModule} from "primeng/button";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { TopSidebarComponent } from './shared/top-sidebar/top-sidebar.component';
import {RippleModule} from "primeng/ripple";
import {InputTextModule} from "primeng/inputtext";
import { FooterCustomComponent } from './shared/footer-custom/footer-custom.component';
import { FeatureComponent } from './feature/feature.component';
import { HomeComponent } from './home/home.component';
@NgModule({
  declarations: [
    AppComponent,
    TopSidebarComponent,
    FooterCustomComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SidebarModule,
    ButtonModule,
    RippleModule,
    InputTextModule,
    BrowserAnimationsModule

  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    BrowserAnimationsModule
  ]
})
export class AppModule { }
