import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-top-sidebar',
  templateUrl: './top-sidebar.component.html',
  styleUrls: ['./top-sidebar.component.scss'],
})
export class TopSidebarComponent implements OnInit {
  visibleBar= true;
  private _event: any;
  constructor() { }
  @ViewChild("sidebar") sidebar: any;
  ngOnInit(): void {
  }
  @HostListener('window:scroll', ['$event']) // for window scroll events
  onScroll(event: any) {
    if(window.pageYOffset !== 0 ) {
      this.sidebar.nativeElement.className = 'change_custom sidebar_custom'
    } else {
      this.sidebar.nativeElement.className = 'sidebar_custom'
    }

  }
}
