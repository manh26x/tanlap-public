import {AfterViewInit, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import { interval, Subscription } from 'rxjs';
import {PrimeNGConfig} from "primeng/api";
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

})
export class AppComponent implements  OnInit{

  constructor(private primengConfig: PrimeNGConfig) {}
  ngOnInit() {
    this.primengConfig.ripple = true;
  }
  title = 'Tân Lập';

}
