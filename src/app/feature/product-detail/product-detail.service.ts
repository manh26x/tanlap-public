import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ProductDetailService {

  private baseUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.baseUrl = environment.baseUrl;
  }



  getProduct(id: any) {
    const requestUrl = `${this.baseUrl}/api/product/get/${id}`;
    return this.http.get<any>(requestUrl);
  }
}
