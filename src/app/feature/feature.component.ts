import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.scss']
})
export class FeatureComponent implements OnInit {


  filterForm: any = {
    name: null,
    kind: null,
    price: null,
    dvt: null,
    rangeValues:  [200000,800000]
  }
  json = JSON;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      const filterForm = JSON.parse(params['filterForm']);
      if(filterForm) {
        this.filterForm = filterForm;
      }
    });
  }

}
