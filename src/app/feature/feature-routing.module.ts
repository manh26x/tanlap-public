import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {FeatureComponent} from "./feature.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ProductDetailComponent} from "./product-detail/product-detail.component";

const routes: Routes = [
  {path: '', component: FeatureComponent,
    children: [
      {path: '', component: DashboardComponent},
      {path: ':id', component: ProductDetailComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeatureRoutingModule { }
