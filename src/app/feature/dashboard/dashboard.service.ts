import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private baseUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.baseUrl = environment.baseUrl;
  }



  getAllProduct() {
    const requestUrl = `${this.baseUrl}/api/product/all`;
    return this.http.get<any>(requestUrl);
  }

}
