import {Component, OnInit, ViewChild} from '@angular/core';
import {PrimeNGConfig, SelectItem} from "primeng/api";
import {Product} from "./product";
import {DashboardService} from "./dashboard.service";
import {ActivatedRoute} from "@angular/router";



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  products: Product[]  = [];

  @ViewChild('dv') dv: any;

  sortOptions: SelectItem[] =[];

  sortOrder: number = 0;

  sortField: string ='';
  sortKey: any;
  productList: Product[] = [];

  constructor( private primengConfig: PrimeNGConfig,
               private dashboardService: DashboardService,
               private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.dashboardService.getAllProduct().subscribe(res => {
      this.productList = res;
      this.route.queryParams.subscribe(params => {
        const filterForm = JSON.parse(params['filterForm']);
        console.log(this.productList);
        this.products = [];
        if(filterForm.name === null) filterForm.name = ''

        this.productList.forEach(p => {
          // @ts-ignore
          if(p.name?.includes(filterForm.name)
            && (filterForm.kind === []||  filterForm.kind.includes(p.kind))
            && (filterForm.rangeValues[0] <= p.price && filterForm.rangeValues[1] >= p.price)
            && (filterForm.dvt.length === 0 || filterForm.dvt.includes(p.dvt))
          ) {
            this.products.push(p);
          }
        })
      }, () => this.products = this.productList);
    });



    this.sortOptions = [
      {label: 'Price High to Low', value: '!price'},
      {label: 'Price Low to High', value: 'price'}
    ];

    this.primengConfig.ripple = true;
  }

  onSortChange(event: { value: any; }) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    }
    else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }

}
