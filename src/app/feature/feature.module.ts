import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureRoutingModule } from './feature-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import {FeatureComponent} from "./feature.component";
import {ButtonModule} from "primeng/button";
import {InputTextModule} from "primeng/inputtext";
import {AccordionModule} from "primeng/accordion";
import {CheckboxModule} from "primeng/checkbox";
import {RippleModule} from "primeng/ripple";
import {DataViewModule} from "primeng/dataview";
import {DropdownModule} from "primeng/dropdown";
import {RatingModule} from "primeng/rating";
import {FormsModule} from "@angular/forms";
import {DashboardService} from "./dashboard/dashboard.service";
import {HttpClientModule} from "@angular/common/http";
import { ProductDetailComponent } from './product-detail/product-detail.component';
import {ProductDetailService} from "./product-detail/product-detail.service";
import {SliderModule} from 'primeng/slider';


@NgModule({
  providers: [DashboardService, ProductDetailService],
  declarations: [
    DashboardComponent,
    FeatureComponent,
    ProductDetailComponent
  ],
  imports: [
    CommonModule,
    FeatureRoutingModule,
    SliderModule,
    ButtonModule,
    InputTextModule,
    AccordionModule,
    CheckboxModule,
    RippleModule,
    DataViewModule,
    DropdownModule,
    RatingModule,
    FormsModule,
    HttpClientModule
  ]
})
export class FeatureModule { }
