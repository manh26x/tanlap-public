import {AfterViewInit, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {interval, Subscription} from "rxjs";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild("brandHeader") brandHeader: any;
  @ViewChild("brandDescription") brandDescription: any;
  @ViewChild("contentProducts") contentProducts: any;
  subscription: Subscription | undefined;
  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    const source = interval(3000);
    let count = 0;
    this.subscription = source.subscribe(val => {
      if(count === 0) {
        this.brandHeader.nativeElement.innerHTML = 'Xây dựng căn nhà trong mơ của bạn';
        this.brandDescription.nativeElement.innerHTML = 'Lối đi, sân trong, bếp ngoài trời,... của bạn sẽ trở nên cực kỳ hấp dẫn.';
        count ++;
      } else {
        this.brandHeader.nativeElement.innerHTML = 'TẠO VƯỜN ƯƠN LÝ TƯỞNG CHO BẠN';
        this.brandDescription.nativeElement.innerHTML = 'Sản phẩm với chất lượng tốt nhất';
        count --;
      }
    });
  }
  @HostListener('window:scroll', ['$event']) // for window scroll events
  onScroll(event: any) {
    if(window.pageYOffset >= 200) {
      this.contentProducts.nativeElement.className = "content-products fade-in"
    } else {
      this.contentProducts.nativeElement.className = "content-products"
    }

  }
}
